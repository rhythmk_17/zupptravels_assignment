class businfo {
  String bus_name;
  String bus_type;
  String bus_seats;
  String bus_dep;
  String bus_arr;
  String bus_fare;

  businfo(
      {this.bus_name,
      this.bus_fare,
      this.bus_type,
      this.bus_seats,
      this.bus_dep,
      this.bus_arr});
}
