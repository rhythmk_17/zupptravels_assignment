import 'package:flutter/material.dart';

class FlightPage extends StatefulWidget {
  @override
  _FlightPageState createState() => _FlightPageState();
}

class _FlightPageState extends State<FlightPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        child: Text(
          'Flight page',
          style: TextStyle(
            color: Colors.indigo[700],
            fontFamily: 'Lato',
            fontSize: 30,
          ),
        ),
      ),
    );
  }
}
