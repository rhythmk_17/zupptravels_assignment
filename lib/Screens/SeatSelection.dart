import 'package:assignment_zupp/Screens/BusPerson.dart';
import 'package:assignment_zupp/Screens/ReservPage.dart';
import 'package:flutter/material.dart';

class SeatSelection extends StatefulWidget {
  static String id = 'seat-selection';
  @override
  _SeatSelectionState createState() => _SeatSelectionState();
}

class _SeatSelectionState extends State<SeatSelection> {
  @override
  bool selec = false;
  List<bool> isSelected = [false, false, false];
  List<bool> tap = [false, false, false, false, false];

  Widget build(BuildContext context) {
    void change(int ie) {
      setState(() {
        tap[ie] = !tap[ie];
      });
      print(selec);
    }

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          'Select Seats',
          style: TextStyle(color: Colors.black, fontFamily: 'LatoB'),
        ),
        backgroundColor: Colors.white,
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            size: 22,
            color: Colors.black,
          ),
          onPressed: () {},
        ),
      ),
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.all(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                      child: Row(
                    children: <Widget>[
                      CircleAvatar(
                        backgroundColor: Color(0xFF818AB2),
                        radius: 10,
                      ),
                      SizedBox(
                        width: 7,
                      ),
                      Text(
                        'Reserved',
                        style: TextStyle(
                          color: Colors.black54,
                          fontFamily: 'LatoB',
                          fontSize: 16,
                        ),
                      ),
                    ],
                  )),
                  Container(
                      child: Row(
                    children: <Widget>[
                      CircleAvatar(
                        backgroundColor: Color(0xFFE6EBFB),
                        radius: 10,
                      ),
                      SizedBox(
                        width: 7,
                      ),
                      Text(
                        'Available',
                        style: TextStyle(
                          color: Colors.black54,
                          fontFamily: 'LatoB',
                          fontSize: 16,
                        ),
                      ),
                    ],
                  )),
                  Container(
                    child: Row(
                      children: <Widget>[
                        CircleAvatar(
                          backgroundColor: Color(0xFF37D2FF),
                          radius: 10,
                        ),
                        SizedBox(
                          width: 7,
                        ),
                        Text(
                          'Selected',
                          style: TextStyle(
                            color: Colors.black54,
                            fontFamily: 'LatoB',
                            fontSize: 16,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 35, horizontal: 25),
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 15,
                    ),

                    SizedBox(
                      height: 15,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            SeatIndi(
                              clr: Color(0xFF37D2FF),
                              value: false,
                            ),
                            SizedBox(
                              width: 17,
                            ),
                            SeatIndi(
                              clr: Color(0xFF37D2FF),
                              value: false,
                            ),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            SeatIndi(
                              clr: Color(0xFF37D2FF),
                              value: false,
                            ),
                            SizedBox(
                              width: 17,
                            ),
                            SeatIndi(
                              clr: Color(0xFF37D2FF),
                              value: false,
                            ),
                            SizedBox(
                              width: 17,
                            ),
                            SeatIndi(
                              clr: Color(0xFF37D2FF),
                              value: false,
                            ),
                          ],
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            SeatIndi(
                              clr: Color(0xFF37D2FF),
                              value: false,
                            ),
                            SizedBox(
                              width: 17,
                            ),
                            SeatIndi(
                              clr: Color(0xFF37D2FF),
                              value: false,
                            ),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            SeatIndi(
                              clr: Color(0xFF37D2FF),
                              value: false,
                            ),
                            SizedBox(
                              width: 17,
                            ),
                            SeatIndi(
                              clr: Color(0xFF37D2FF),
                              value: false,
                            ),
                            SizedBox(
                              width: 17,
                            ),
                            SeatIndi(
                              clr: Color(0xFF37D2FF),
                              value: false,
                            ),
                          ],
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            SeatIndi(
                              clr: Color(0xFF37D2FF),
                              value: false,
                            ),
                            SizedBox(
                              width: 17,
                            ),
                            SeatIndi(
                              clr: Color(0xFF37D2FF),
                              value: false,
                            ),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            SeatIndi(
                              clr: Color(0xFF37D2FF),
                              value: false,
                            ),
                            SizedBox(
                              width: 17,
                            ),
                            SeatIndi(
                              clr: Color(0xFF37D2FF),
                              value: false,
                            ),
                            SizedBox(
                              width: 17,
                            ),
                            SeatIndi(
                              clr: Color(0xFF37D2FF),
                              value: false,
                            ),
                          ],
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            SeatIndi(
                              clr: Color(0xFF37D2FF),
                              value: false,
                            ),
                            SizedBox(
                              width: 17,
                            ),
                            SeatIndi(
                              clr: Color(0xFF37D2FF),
                              value: false,
                            ),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            SeatIndi(
                              clr: Color(0xFF37D2FF),
                              value: false,
                            ),
                            SizedBox(
                              width: 17,
                            ),
                            SeatIndi(
                              clr: Color(0xFF37D2FF),
                              value: false,
                            ),
                            SizedBox(
                              width: 17,
                            ),
                            SeatIndi(
                              clr: Color(0xFF37D2FF),
                              value: false,
                            ),
                          ],
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            SeatIndi(
                              clr: Color(0xFF37D2FF),
                              value: false,
                            ),
                            SizedBox(
                              width: 17,
                            ),
                            SeatIndi(
                              clr: Color(0xFF37D2FF),
                              value: false,
                            ),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            SeatIndi(
                              clr: Color(0xFF37D2FF),
                              value: false,
                            ),
                            SizedBox(
                              width: 17,
                            ),
                            SeatIndi(
                              clr: Color(0xFF37D2FF),
                              value: false,
                            ),
                            SizedBox(
                              width: 17,
                            ),
                            SeatIndi(
                              clr: Color(0xFF37D2FF),
                              value: false,
                            ),
                          ],
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            SeatIndi(
                              clr: Color(0xFF37D2FF),
                              value: false,
                            ),
                            SizedBox(
                              width: 17,
                            ),
                            SeatIndi(
                              clr: Color(0xFF37D2FF),
                              value: false,
                            ),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            SeatIndi(
                              clr: Color(0xFF37D2FF),
                              value: false,
                            ),
                            SizedBox(
                              width: 17,
                            ),
                            SeatIndi(
                              clr: Color(0xFF37D2FF),
                              value: false,
                            ),
                            SizedBox(
                              width: 17,
                            ),
                            SeatIndi(
                              clr: Color(0xFF37D2FF),
                              value: false,
                            ),
                          ],
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            SeatIndi(
                              clr: Color(0xFF37D2FF),
                              value: false,
                            ),
                            SizedBox(
                              width: 17,
                            ),
                            SeatIndi(
                              clr: Color(0xFF37D2FF),
                              value: false,
                            ),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            SeatIndi(
                              clr: Color(0xFF37D2FF),
                              value: false,
                            ),
                            SizedBox(
                              width: 17,
                            ),
                            SeatIndi(
                              clr: Color(0xFF37D2FF),
                              value: false,
                            ),
                            SizedBox(
                              width: 17,
                            ),
                            SeatIndi(
                              clr: Color(0xFF37D2FF),
                              value: false,
                            ),
                          ],
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            SeatIndi(
                              clr: Color(0xFF37D2FF),
                              value: false,
                            ),
                            SizedBox(
                              width: 17,
                            ),
                            SeatIndi(
                              clr: Color(0xFF37D2FF),
                              value: false,
                            ),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            SeatIndi(
                              clr: Color(0xFF37D2FF),
                              value: false,
                            ),
                            SizedBox(
                              width: 17,
                            ),
                            SeatIndi(
                              clr: Color(0xFF37D2FF),
                              value: false,
                            ),
                            SizedBox(
                              width: 17,
                            ),
                            SeatIndi(
                              clr: Color(0xFF37D2FF),
                              value: false,
                            ),
                          ],
                        ),
                      ],
                    ),

                    // ToggleButtons(
                    //   children: <Widget>[
                    //     Icon(Icons.ac_unit),
                    //     Icon(Icons.call),
                    //     Icon(Icons.cake),
                    //   ],
                    //   onPressed: (int index) {
                    //     setState(() {
                    //       isSelected[index] = !isSelected[index];
                    //     });
                    //   },
                    //   selectedColor: Colors.red,
                    //   isSelected: isSelected,
                    // ),
                    // SeatRow(
                    //   clr1: Color(0xFF818AB2),
                    // ),
                    // SizedBox(
                    //   height: 15,
                    // ),
                    // SeatRow(clr1: Color(0xFF818AB2)),
                    // SizedBox(
                    //   height: 15,
                    // ),
                    // SeatRow(
                    //   clr1: Color(0xFF818AB2),
                    // ),
                    // SizedBox(
                    //   height: 15,
                    // ),
                    // SeatRow(clr1: Color(0xFF818AB2)),
                    // SizedBox(
                    //   height: 15,
                    // ),
                    // SeatRow(
                    //   clr1: Color(0xFF818AB2),
                    // ),
                    // SizedBox(
                    //   height: 15,
                    // ),
                    // SeatRow(clr1: Color(0xFF818AB2)),
                  ],
                ),
              ),
              Container(
                height: 100,
                padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                width: double.infinity,
                decoration: BoxDecoration(
                    color: Color(0xFFE6EBFB),
                    borderRadius: BorderRadius.circular(10)),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          'Selected Seats',
                          style: TextStyle(
                            color: Colors.black54,
                            fontFamily: 'LatoB',
                            fontSize: 18,
                          ),
                        ),
                        Text(
                          'No. of Seats',
                          style: TextStyle(
                            color: Colors.black54,
                            fontFamily: 'LatoB',
                            fontSize: 16,
                          ),
                        ),
                        Text(
                          'Total Fare',
                          style: TextStyle(
                            color: Colors.black54,
                            fontFamily: 'LatoB',
                            fontSize: 16,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          'L1,L2',
                          style: TextStyle(
                            color: Colors.black,
                            fontFamily: 'LatoB',
                            fontSize: 18,
                          ),
                        ),
                        Text(
                          '2',
                          style: TextStyle(
                            color: Colors.black,
                            fontFamily: 'LatoB',
                            fontSize: 16,
                          ),
                        ),
                        Text(
                          'Rs. 2100',
                          style: TextStyle(
                            color: Colors.black,
                            fontFamily: 'LatoB',
                            fontSize: 16,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Center(
                          child: GestureDetector(
                            onTap: () {
                              Navigator.pushNamed(context, BusPerson.id);
                            },
                            child: Container(
                              width: 120,
                              height: 30,
                              decoration: BoxDecoration(
                                color: Color(0xFF37D2FF),
                                borderRadius: BorderRadius.circular(25),
                              ),
                              child: Center(
                                child: Text(
                                  'CONFIRM',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontFamily: 'LatoB',
                                    fontSize: 13,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class seats extends StatelessWidget {
  Color clr;
  bool isSeat;
  Function press;

  seats({this.clr, this.isSeat, this.press});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: press,
      child: Container(
        height: 27,
        width: 27,
        decoration: BoxDecoration(
            color: isSeat ? Color(0xFFE6EBFB) : clr,
            borderRadius: BorderRadius.circular(5)),
      ),
    );
  }
}

class SeatIndi extends StatefulWidget {
  bool value;
  final Color clr;
  SeatIndi({this.clr, this.value});
  @override
  _SeatIndiState createState() => _SeatIndiState();
}

class _SeatIndiState extends State<SeatIndi> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setState(() {
          widget.value = !widget.value;
        });
      },
      child: Container(
        height: 27,
        width: 27,
        decoration: BoxDecoration(
            color: widget.value ? widget.clr : Color(0xFFE6EBFB),
            borderRadius: BorderRadius.circular(5)),
      ),
    );
  }
}
