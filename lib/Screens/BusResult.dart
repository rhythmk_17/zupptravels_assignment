import 'package:assignment_zupp/Screens/ReservPage.dart';
import 'package:assignment_zupp/Screens/SeatSelection.dart';
import 'package:assignment_zupp/modals/businfo.dart';
import 'package:flutter/material.dart';

class BusPager extends StatefulWidget {
  static String id = 'bus-result';
  @override
  _BusPagerState createState() => _BusPagerState();
}

class _BusPagerState extends State<BusPager> {
  List<bool> isSelected = [false, false, false, false, false];

  List<businfo> bus_list = [
    businfo(
        bus_name: 'Shreenathji Travels',
        bus_fare: '700',
        bus_type: 'AC Sleeper 1x1',
        bus_seats: '10 seats',
        bus_arr: '5:00 PM',
        bus_dep: '11:00 AM'),
    businfo(
        bus_name: 'Eagle Travels',
        bus_fare: '1400',
        bus_type: 'AC Sleeper 2x1',
        bus_seats: '12 seats',
        bus_arr: '7:00 PM',
        bus_dep: '11:00 AM'),
    businfo(
        bus_name: 'Shreenathji Travels',
        bus_fare: '300',
        bus_type: 'NON AC Sleeper 1x1',
        bus_seats: '15 seats',
        bus_arr: '6:00 PM',
        bus_dep: '1:00 PM'),
    businfo(
        bus_name: 'Paulo Travels',
        bus_fare: '700',
        bus_type: 'AC Sleeper 1x1',
        bus_seats: '7 seats',
        bus_arr: '4:00 PM',
        bus_dep: '10:00 AM'),
    businfo(
        bus_name: 'Falcon Travels',
        bus_fare: '600',
        bus_type: 'AC Sleeper 1x1',
        bus_seats: '5 seats',
        bus_arr: '5:00 PM',
        bus_dep: '11:00 AM'),
    businfo(
        bus_name: 'Brahmani Travels',
        bus_fare: '500',
        bus_type: 'AC Sleeper 1x1',
        bus_seats: '22 seats',
        bus_arr: '6:30 PM',
        bus_dep: '1:00 PM'),
    businfo(
        bus_name: 'Shreenathji Travels',
        bus_fare: '700',
        bus_type: 'AC Sleeper 1x1',
        bus_seats: '12',
        bus_arr: '5:00 PM',
        bus_dep: '11:00 AM'),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          'Bus Booking',
          style: TextStyle(color: Colors.black, fontFamily: 'LatoB'),
        ),
        backgroundColor: Colors.white,
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            size: 22,
            color: Colors.black,
          ),
          onPressed: () {},
        ),
      ),
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.symmetric(vertical: 5, horizontal: 15),
              child: GestureDetector(
                onTap: () {
                  Navigator.pushNamed(context, SeatSelection.id);
                },
                child: Container(
                    decoration: BoxDecoration(
                        color: Color(0xFF37D2FF),
                        borderRadius: BorderRadius.circular(10)),
                    padding: EdgeInsets.all(15),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'Surat - Pune',
                              style: TextStyle(
                                color: Colors.white,
                                fontFamily: 'LatoB',
                                fontSize: 20,
                              ),
                            ),
                            SizedBox(height: 5),
                            Text(
                              'One way',
                              style: TextStyle(
                                color: Colors.black45,
                                fontFamily: 'Lato',
                                fontSize: 15,
                              ),
                            ),
                          ],
                        ),
                        Text(
                          '24 Apr,2020',
                          style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'Lato',
                            fontSize: 16,
                          ),
                        ),
                      ],
                    )),
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        isSelected[0] = !isSelected[0];
                      });
                    },
                    child: Container(
                      padding: EdgeInsets.all(7),
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.black45),
                        color: isSelected[0] ? Color(0xFF37D2FF) : Colors.white,
                        borderRadius: BorderRadius.circular(25),
                      ),
                      child: Center(
                        child: Text(
                          'Before 9:00 AM',
                          style: TextStyle(
                            color: isSelected[0]
                                ? Colors.white
                                : Color(0xFF37D2FF),
                            fontFamily: 'Lato',
                            fontSize: 12,
                          ),
                        ),
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        isSelected[1] = !isSelected[1];
                      });
                    },
                    child: Container(
                      padding: EdgeInsets.all(7),
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.black45),
                        color: isSelected[1] ? Color(0xFF37D2FF) : Colors.white,
                        borderRadius: BorderRadius.circular(25),
                      ),
                      child: Center(
                        child: Text(
                          '9:00 PM - 9:00 AM',
                          style: TextStyle(
                            color: isSelected[1]
                                ? Colors.white
                                : Color(0xFF37D2FF),
                            fontFamily: 'Lato',
                            fontSize: 12,
                          ),
                        ),
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        isSelected[2] = !isSelected[2];
                      });
                    },
                    child: Container(
                      padding: EdgeInsets.all(7),
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.black45),
                        color: isSelected[2] ? Color(0xFF37D2FF) : Colors.white,
                        borderRadius: BorderRadius.circular(25),
                      ),
                      child: Center(
                        child: Text(
                          '9:00 PM - 1:00 AM',
                          style: TextStyle(
                            color: isSelected[2]
                                ? Colors.white
                                : Color(0xFF37D2FF),
                            fontFamily: 'Lato',
                            fontSize: 12,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        isSelected[3] = !isSelected[3];
                      });
                    },
                    child: Container(
                      padding: EdgeInsets.all(7),
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.black45),
                        color: isSelected[3] ? Color(0xFF37D2FF) : Colors.white,
                        borderRadius: BorderRadius.circular(25),
                      ),
                      child: Center(
                        child: Text(
                          'AC VOLVO',
                          style: TextStyle(
                            color: isSelected[3]
                                ? Colors.white
                                : Color(0xFF37D2FF),
                            fontFamily: 'Lato',
                            fontSize: 12,
                          ),
                        ),
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        isSelected[4] = !isSelected[4];
                      });
                    },
                    child: Container(
                      width: 170,
                      padding: EdgeInsets.all(7),
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.black45),
                        color: isSelected[4] ? Color(0xFF37D2FF) : Colors.white,
                        borderRadius: BorderRadius.circular(25),
                      ),
                      child: Center(
                        child: Text(
                          'MORE FILTERS',
                          style: TextStyle(
                            color: isSelected[4]
                                ? Colors.white
                                : Color(0xFF37D2FF),
                            fontFamily: 'Lato',
                            fontSize: 14,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Expanded(
              child: ListView.builder(
                itemCount: bus_list.length,
                itemBuilder: (context, index) {
                  return BusCard(
                    nm: bus_list[index].bus_name,
                    st: bus_list[index].bus_seats,
                    dep: bus_list[index].bus_dep,
                    arr: bus_list[index].bus_arr,
                    pr: bus_list[index].bus_fare,
                    ty: bus_list[index].bus_type,
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class BusCard extends StatelessWidget {
  String nm;
  String pr;
  String arr;
  String dep;
  String ty;
  String st;

  BusCard({this.nm, this.ty, this.pr, this.arr, this.dep, this.st});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {},
      child: Card(
        child: Container(
          padding: EdgeInsets.all(10),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    '$nm',
                    style: TextStyle(
                      color: Colors.black,
                      fontFamily: 'LatoB',
                      fontSize: 18,
                    ),
                  ),
                  Text(
                    '$pr',
                    style: TextStyle(
                      color: Color(0xFF37D2FF),
                      fontFamily: 'LatoB',
                      fontSize: 19,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    '$ty',
                    style: TextStyle(
                      color: Colors.black45,
                      fontFamily: 'Lato',
                      fontSize: 14,
                    ),
                  ),
                  Text(
                    '$dep',
                    style: TextStyle(
                      color: Colors.black,
                      fontFamily: 'LatoB',
                      fontSize: 14,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 7,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    '$st',
                    style: TextStyle(
                      color: Colors.black45,
                      fontFamily: 'Lato',
                      fontSize: 14,
                    ),
                  ),
                  Text(
                    '$arr',
                    style: TextStyle(
                      color: Colors.black45,
                      fontFamily: 'LatoB',
                      fontSize: 14,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
