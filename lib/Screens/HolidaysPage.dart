import 'package:assignment_zupp/Screens/VacationFirst.dart';
import 'package:assignment_zupp/Screens/VacationPage.dart';
import 'package:flutter/material.dart';
import 'package:assignment_zupp/widgets/constants.dart';

class HolidaysPage extends StatefulWidget {
  @override
  _HolidaysPageState createState() => _HolidaysPageState();
}

class _HolidaysPageState extends State<HolidaysPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: SafeArea(
          child: Padding(
            padding: EdgeInsets.all(15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                TextFormField(
                  enabled: true,
                  style: TextStyle(
                    fontFamily: 'Lato',
                  ),
                  onChanged: (value) {
                    //Do something with the user input.
                  },
                  decoration: borderdecor.copyWith(
                    hintText: 'Search for packages',
                    prefixIcon: Icon(
                      Icons.search,
                      color: Color(0xFF2687EE),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(30.0)),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide:
                          BorderSide(color: Color(0xFF37D2FF), width: 1.5),
                      borderRadius: BorderRadius.all(Radius.circular(30.0)),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide:
                          BorderSide(color: Color(0xFF37D2FF), width: 2.2),
                      borderRadius: BorderRadius.all(Radius.circular(30.0)),
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Center(
                  child: Container(
                    child: Text(
                      'UPCOMING TRIPS',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.black,
                        fontFamily: 'LatoB',
                        fontSize: 23,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Container(
                  height: 260,
                  child: ListView.builder(
                      padding: EdgeInsets.only(right: 10),
                      scrollDirection: Axis.horizontal,
                      itemCount: 5,
                      itemBuilder: (_, index) {
                        return UpTrips();
                      }),
                ),
                SizedBox(
                  height: 15,
                ),
                Center(
                  child: Container(
                    child: Text(
                      'STYLE TOURS',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.black,
                        fontFamily: 'LatoB',
                        fontSize: 23,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Container(
                  height: 160,
                  child: ListView.builder(
                      padding: EdgeInsets.only(right: 10),
                      scrollDirection: Axis.horizontal,
                      itemCount: 5,
                      itemBuilder: (_, index) {
                        return StyTours();
                      }),
                ),
                SizedBox(
                  height: 15,
                ),
                Center(
                  child: Container(
                    child: Text(
                      'BUDGET TOURS',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.black,
                        fontFamily: 'LatoB',
                        fontSize: 23,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Container(
                  height: 150,
                  child: ListView.builder(
                      padding: EdgeInsets.only(right: 10),
                      scrollDirection: Axis.horizontal,
                      itemCount: 5,
                      itemBuilder: (_, index) {
                        return BudTours();
                      }),
                ),
                SizedBox(
                  height: 15,
                ),
                Center(
                  child: Container(
                    child: Text(
                      'SEASON\'S TOP DESTINATIONS',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.black,
                        fontFamily: 'LatoB',
                        fontSize: 23,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  height: 200,
                  child: ListView.builder(
                      padding: EdgeInsets.only(right: 10),
                      scrollDirection: Axis.horizontal,
                      itemCount: 5,
                      itemBuilder: (_, index) {
                        return SeasTop();
                      }),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class UpTrips extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, VacationFirst.id);
      },
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
        width: 200,
        decoration: BoxDecoration(boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 2,
            blurRadius: 4,
          ),
        ], borderRadius: BorderRadius.circular(15), color: Colors.white),
        child: Column(
          children: <Widget>[
            Container(
              width: double.infinity,
              decoration: BoxDecoration(
                color: Colors.cyan[50],
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(15),
                    topRight: Radius.circular(15)),
              ),
              child: Center(
                child: Text(
                  '12-04-2020',
                  style: TextStyle(
                    color: Colors.black,
                    fontFamily: 'LatoB',
                    fontSize: 12,
                  ),
                ),
              ),
              height: 30,
            ),
            Container(
              height: 140,
              width: double.infinity,
              child: Image(
                image: AssetImage('assets/images/1.jpg'),
                fit: BoxFit.cover,
              ),
            ),
            Container(
              height: 25,
              color: Colors.cyan[50],
              width: double.infinity,
              padding: EdgeInsets.only(left: 10, top: 5),
              child: Text(
                'GRAND GOA',
                style: TextStyle(
                  color: Colors.black,
                  fontFamily: 'LatoB',
                  fontSize: 13,
                ),
              ),
            ),
            Container(
                color: Colors.cyan[50],
                width: double.infinity,
                padding:
                    EdgeInsets.only(left: 10, right: 10, bottom: 5, top: 5),
                child: Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        '6 Days',
                        style: TextStyle(
                          color: Colors.black,
                          fontFamily: 'LatoB',
                          fontSize: 13,
                        ),
                      ),
                      Text(
                        'INDIA',
                        style: TextStyle(
                          color: Colors.black,
                          fontFamily: 'LatoB',
                          fontSize: 13,
                        ),
                      ),
                    ],
                  ),
                )),
            Container(
              padding: EdgeInsets.only(top: 5),
              width: double.infinity,
              child: Center(
                child: Text(
                  'Rs. 12,000',
                  style: TextStyle(
                    color: Color(0xFF37D2FF),
                    fontFamily: 'LatoB',
                    fontSize: 15,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class StyTours extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 250,
      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      decoration: BoxDecoration(boxShadow: [
        BoxShadow(
          color: Colors.grey.withOpacity(0.5),
          spreadRadius: 2,
          blurRadius: 4,
        ),
      ], borderRadius: BorderRadius.circular(15), color: Colors.white),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            height: 120,
            width: double.infinity,
            child: Image(
              image: AssetImage(
                'assets/images/7.jpg',
              ),
              fit: BoxFit.fill,
            ),
          ),
          Container(
            height: 30,
            child: Center(
              child: Text(
                'MOUNTAINS',
                style: TextStyle(
                  color: Colors.black,
                  fontFamily: 'LatoB',
                  fontSize: 15,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}

class BudTours extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
        width: 230,
        decoration: BoxDecoration(boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 2,
            blurRadius: 4,
          ),
        ], borderRadius: BorderRadius.circular(15), color: Colors.white),
        child: Image(
          image: AssetImage(
            'assets/images/6.jpg',
          ),
          height: 150,
          width: 230,
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}

class SeasTop extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
        width: 150,
        child: Column(
          children: <Widget>[
            CircleAvatar(
              radius: 50,
              backgroundImage: (AssetImage('assets/images/4.jpg')),
            ),
            SizedBox(
              height: 10,
            ),
            Center(
              child: Text(
                'Washington',
                style: TextStyle(
                  color: Colors.black,
                  fontFamily: 'LatoB',
                  fontSize: 17,
                ),
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Center(
              child: Text(
                'USA',
                style: TextStyle(
                  color: Colors.black,
                  fontFamily: 'LatoB',
                  fontSize: 15,
                ),
              ),
            ),
          ],
        ));
  }
}
