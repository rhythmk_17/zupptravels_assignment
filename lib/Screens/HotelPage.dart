import 'package:flutter/material.dart';

class HotelPage extends StatefulWidget {
  @override
  _HotelPageState createState() => _HotelPageState();
}

class _HotelPageState extends State<HotelPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        child: Text(
          'Hotel page',
          style: TextStyle(
            color: Colors.indigo[700],
            fontFamily: 'Lato',
            fontSize: 30,
          ),
        ),
      ),
    );
  }
}
