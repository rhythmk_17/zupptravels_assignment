import 'package:assignment_zupp/Screens/VacationPage.dart';
import 'package:flutter/material.dart';

class VacationFirst extends StatefulWidget {
  @override
  static String id = 'vacation-first';
  _VacationFirstState createState() => _VacationFirstState();
}

class _VacationFirstState extends State<VacationFirst> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      // appBar: AppBar(
      //   title: Text(
      //     'Holiday details',
      //     style:
      //         TextStyle(color: Colors.black, fontFamily: 'LatoB', fontSize: 20),
      //   ),
      //   backgroundColor: Colors.white,
      //   elevation: 0,
      // ),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Column(
              children: <Widget>[
                Container(
                  height: 300,
                  width: double.infinity,
                  child: Image(
                    image: AssetImage('assets/images/5.jpg'),
                    fit: BoxFit.cover,
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                DetailsVac(),
              ],
            ),
            GestureDetector(
              onTap: () {
                Navigator.pushNamed(context, VacationPage.id);
              },
              child: Container(
                decoration: BoxDecoration(
                  color: Color(0xFF37D2FF),
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(15),
                      bottomRight: Radius.circular(15)),
                ),
                height: 50,
                child: Center(
                  child: Text(
                    'PROCEED',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.white, fontFamily: 'LatoB', fontSize: 17),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class DetailsVac extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(15),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 2,
              blurRadius: 4,
            ),
          ],
        ),
        margin: EdgeInsets.all(15),
        child: Column(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                color: Color(0xFF37D2FF),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(15),
                    topRight: Radius.circular(15)),
              ),
              height: 50,
              child: Center(
                child: Text(
                  'LOWEST PRICE GUARANTEE',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.white, fontFamily: 'LatoB', fontSize: 17),
                ),
              ),
            ),
            Container(
              height: 50,
              child: Center(
                child: Text(
                  'INR 5000',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Color(0xFF37D2FF),
                      fontFamily: 'LatoB',
                      fontSize: 17),
                ),
              ),
            ),
            Divider(
              height: 0,
              thickness: 1,
            ),
            Container(
              padding: EdgeInsets.all(15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        '2 ADULTS',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Color(0xFF37D2FF),
                            fontFamily: 'LatoB',
                            fontSize: 15),
                      ),
                      SizedBox(
                        height: 7,
                      ),
                      Text(
                        '31 DAYS',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Color(0xFF37D2FF),
                            fontFamily: 'LatoB',
                            fontSize: 15),
                      ),
                      SizedBox(
                        height: 7,
                      ),
                      Text(
                        'HELLO VACATION',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Color(0xFF37D2FF),
                            fontFamily: 'LatoB',
                            fontSize: 15),
                      ),
                    ],
                  ),
                  VerticalDivider(
                    thickness: 1,
                    width: 0,
                    color: Colors.black45,
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'ABCDEF',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Color(0xFF37D2FF),
                            fontFamily: 'LatoB',
                            fontSize: 15),
                      ),
                    ],
                  )
                ],
              ),
            ),
            Divider(
              height: 0,
              thickness: 1,
            ),
            SizedBox(
              height: 20,
            ),
            Center(
              child: GestureDetector(
                onTap: () {},
                child: Container(
                  width: 160,
                  height: 45,
                  decoration: BoxDecoration(
                    color: Color(0xFF37D2FF),
                    borderRadius: BorderRadius.circular(25),
                  ),
                  child: Center(
                    child: Text(
                      'BOOK NOW',
                      style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'LatoB',
                        fontSize: 17,
                      ),
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
          ],
        ));
  }
}
