import 'package:assignment_zupp/modals/destination.dart';
import 'package:assignment_zupp/widgets/dest_card.dart';
import 'package:assignment_zupp/widgets/detailscard.dart';
import 'package:assignment_zupp/widgets/icon_tile.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Homepage extends StatefulWidget {
  static String id = 'homepage';
  @override
  _HomepageState createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  final _fire = Firestore.instance;
  List<Destination> places = [];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        // title: Text('HomePage'),
        backgroundColor: Colors.white,
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.menu,
            size: 27,
            color: Colors.black,
          ),
          onPressed: () {},
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.person_outline,
              color: Colors.black,
              size: 27,
            ),
            onPressed: () {
              // do something
            },
          )
        ],
      ),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(
                height: 25,
              ),
              Padding(
                padding: EdgeInsets.only(left: 25),
                child: RichText(
                  text: TextSpan(
                      children: [
                        TextSpan(
                            text: 'Hello, ',
                            style: TextStyle(
                                fontSize: 32,
                                fontFamily: 'Lato',
                                fontWeight: FontWeight.w700,
                                color: Colors.indigo[700])),
                        TextSpan(text: 'what are you\nlooking for?')
                      ],
                      style: TextStyle(
                          fontSize: 32,
                          fontFamily: 'Lato',
                          fontWeight: FontWeight.w600,
                          color: Colors.black)),
                ),
              ),
              SizedBox(
                height: 27,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  IconTile(
                    iconsym: Icons.home,
                    des: 'Accomodation',
                  ),
                  IconTile(
                    iconsym: Icons.local_taxi,
                    des: 'Cab',
                  ),
                  IconTile(
                    iconsym: Icons.directions,
                    des: 'Adventures',
                  ),
                  IconTile(
                    iconsym: Icons.flight,
                    des: 'Flights',
                  ),
                ],
              ),
              SizedBox(
                height: 30,
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 25),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      'Best Packages',
                      style: TextStyle(
                          fontSize: 25,
                          fontFamily: 'Lato',
                          fontWeight: FontWeight.w600,
                          color: Colors.black),
                    ),
                    IconButton(
                      icon: Icon(
                        Icons.more_horiz,
                        size: 27,
                        color: Colors.black,
                      ),
                      onPressed: () {},
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 20,
              ),
              StreamBuilder(
                  stream: _fire.collection('Vacations').snapshots(),
                  builder: (context, snapshot) {
                    if (!snapshot.hasData) {
                      return Center(
                        child: CircularProgressIndicator(
                          backgroundColor: Colors.blueAccent,
                        ),
                      );
                    }
                    final vacations = snapshot.data.documents;
                    for (var vacation in vacations) {
                      final nm = vacation.data['pname'];
                      final dys = vacation.data['days'];
                      final pr = vacation.data['price'];
                      final imgc = vacation.data['imgcode'];
                      var pv = Destination(
                          days: dys, img: imgc, name: nm, price: pr);
                      places.add(pv);
                    }
                    return Container(
                      margin: EdgeInsets.only(left: 20),
                      height: 250,
                      child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: places.length,
                          itemBuilder: (_, index) {
                            return ImageCard(
                              dest: places[index],
                              name: places[index].name,
                              days: places[index].days,
                              img: places[index].img,
                            );
                          }),
                    );
                  }),
              SizedBox(
                height: 25,
              ),
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 35),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    detail_tile(
                      color: Colors.indigo[100],
                      des: 'Best deals',
                      iconsym: Icons.local_offer,
                    ),
                    detail_tile(
                      color: Colors.pink[100],
                      des: 'Easy payments',
                      iconsym: Icons.payment,
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 35),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    detail_tile(
                      color: Colors.orange[100],
                      des: 'E-wallet',
                      iconsym: Icons.account_balance_wallet,
                    ),
                    detail_tile(
                      color: Colors.greenAccent[100],
                      des: 'Flexible',
                      iconsym: Icons.star,
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
