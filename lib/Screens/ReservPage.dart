import 'package:flutter/material.dart';
import 'package:assignment_zupp/Screens/BusResult.dart';

class ReservPage extends StatefulWidget {
  static String id = 'reserve-page';
  @override
  _ReservPageState createState() => _ReservPageState();
}

class _ReservPageState extends State<ReservPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          'Reservation Details',
          style: TextStyle(color: Colors.black, fontFamily: 'LatoB'),
        ),
        backgroundColor: Colors.white,
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            size: 22,
            color: Colors.black,
          ),
          onPressed: () {},
        ),
      ),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Column(
            children: <Widget>[
              Container(
                width: double.infinity,
                color: Color(0xFFF3F3F3),
                padding: EdgeInsets.all(17),
                child: Center(
                  child: Text(
                    'Confirmed Date: Sat,25th December 2020',
                    style: TextStyle(
                      color: Colors.black54,
                      fontFamily: 'LatoB',
                      fontSize: 17,
                    ),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 15, horizontal: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'From:',
                          style: TextStyle(
                            color: Colors.black54,
                            fontFamily: 'LatoB',
                            fontSize: 17,
                          ),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Text(
                          'Surat',
                          style: TextStyle(
                            color: Colors.black,
                            fontFamily: 'LatoB',
                            fontSize: 17,
                          ),
                        ),
                      ],
                    ),
                    Icon(
                      Icons.arrow_forward,
                      color: Color(0xFF37D2FF),
                      size: 23,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'To:',
                          style: TextStyle(
                            color: Colors.black54,
                            fontFamily: 'LatoB',
                            fontSize: 17,
                          ),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Text(
                          'Ahmedabad',
                          style: TextStyle(
                            color: Colors.black,
                            fontFamily: 'LatoB',
                            fontSize: 17,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Divider(
                  height: 1,
                  thickness: 2,
                  color: Color(0xFFF3F3F3),
                ),
              ),
              Container(
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.black12),
                      borderRadius: BorderRadius.circular(10)),
                  margin: EdgeInsets.symmetric(horizontal: 25, vertical: 25),
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            'Bus Operator',
                            style: TextStyle(
                              color: Colors.black,
                              fontFamily: 'LatoB',
                              fontSize: 16,
                            ),
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            'Boarding Time',
                            style: TextStyle(
                              color: Colors.black,
                              fontFamily: 'LatoB',
                              fontSize: 16,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 12,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            'Paulo Travels',
                            style: TextStyle(
                              color: Color(0xFF37D2FF),
                              fontFamily: 'LatoB',
                              fontSize: 16,
                            ),
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            '4:30 PM',
                            style: TextStyle(
                              color: Color(0xFF37D2FF),
                              fontFamily: 'LatoB',
                              fontSize: 16,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            'Ticket ID',
                            style: TextStyle(
                              color: Colors.black,
                              fontFamily: 'LatoB',
                              fontSize: 16,
                            ),
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            'Seat No',
                            style: TextStyle(
                              color: Colors.black,
                              fontFamily: 'LatoB',
                              fontSize: 16,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 12,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            'PA123478',
                            style: TextStyle(
                              color: Color(0xFF37D2FF),
                              fontFamily: 'LatoB',
                              fontSize: 16,
                            ),
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            '10,11',
                            style: TextStyle(
                              color: Color(0xFF37D2FF),
                              fontFamily: 'LatoB',
                              fontSize: 16,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            'Age',
                            style: TextStyle(
                              color: Colors.black,
                              fontFamily: 'LatoB',
                              fontSize: 16,
                            ),
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            'No.of passengers',
                            style: TextStyle(
                              color: Colors.black,
                              fontFamily: 'LatoB',
                              fontSize: 16,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 12,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            '25 years',
                            style: TextStyle(
                              color: Color(0xFF37D2FF),
                              fontFamily: 'LatoB',
                              fontSize: 16,
                            ),
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            '2',
                            style: TextStyle(
                              color: Color(0xFF37D2FF),
                              fontFamily: 'LatoB',
                              fontSize: 16,
                            ),
                          ),
                        ],
                      ),
                    ],
                  )),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 25, vertical: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      'Mr. Ridham Patel',
                      style: TextStyle(
                        color: Colors.black,
                        fontFamily: 'LatoB',
                        fontSize: 16,
                      ),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      'Total Fare',
                      style: TextStyle(
                        color: Colors.black45,
                        fontFamily: 'LatoB',
                        fontSize: 16,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 25, vertical: 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Text(
                      'Rs. 1500',
                      style: TextStyle(
                        color: Colors.black,
                        fontFamily: 'LatoB',
                        fontSize: 18,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                height: 170,
                width: 170,
                margin: EdgeInsets.only(top: 20),
                child: Image(
                  fit: BoxFit.fill,
                  image: AssetImage('assets/images/bread.png'),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              GestureDetector(
                onTap: () {
                  Navigator.pushNamed(context, BusPager.id);
                },
                child: Container(
                  width: 180,
                  height: 45,
                  decoration: BoxDecoration(
                    color: Color(0xFF37D2FF),
                    borderRadius: BorderRadius.circular(25),
                  ),
                  child: Center(
                    child: Text(
                      'DOWNLOAD',
                      style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'LatoB',
                        fontSize: 18,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
