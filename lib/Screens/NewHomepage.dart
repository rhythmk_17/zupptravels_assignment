import 'package:assignment_zupp/Screens/AppDrawer.dart';
import 'package:flutter/material.dart';
import 'package:assignment_zupp/Screens/SlidableDrawer.dart';
import 'package:assignment_zupp/modals/destination.dart';
import 'package:assignment_zupp/widgets/dest_card.dart';
import 'package:assignment_zupp/widgets/detailscard.dart';
import 'package:assignment_zupp/widgets/icon_tile.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:assignment_zupp/widgets/constants.dart';

class NewHomepage extends StatefulWidget {
  @override
  _NewHomepageState createState() => _NewHomepageState();
}

class _NewHomepageState extends State<NewHomepage> {
  final _fire = Firestore.instance;

  List<Destination> places = [];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                child: TextFormField(
                  enabled: true,
                  style: TextStyle(
                    fontFamily: 'Lato',
                  ),
                  onChanged: (value) {
                    //Do something with the user input.
                  },
                  decoration: borderdecor.copyWith(
                    hintText: 'Search',
                    prefixIcon: Icon(
                      Icons.search,
                      color: Color(0xFF2687EE),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(30.0)),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide:
                          BorderSide(color: Color(0xFF37D2FF), width: 1.5),
                      borderRadius: BorderRadius.all(Radius.circular(30.0)),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide:
                          BorderSide(color: Color(0xFF37D2FF), width: 2.2),
                      borderRadius: BorderRadius.all(Radius.circular(30.0)),
                    ),
                  ),
                ),
              ),
              // SizedBox(
              //   height: 25,
              // ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      'Best Packages',
                      style: TextStyle(
                          fontSize: 23,
                          fontFamily: 'Lato',
                          fontWeight: FontWeight.w600,
                          color: Colors.black),
                    ),
                    IconButton(
                      icon: Icon(
                        Icons.more_horiz,
                        size: 27,
                        color: Colors.black,
                      ),
                      onPressed: () {},
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 20,
              ),
              StreamBuilder(
                  stream: _fire.collection('Vacations').snapshots(),
                  builder: (context, snapshot) {
                    if (!snapshot.hasData) {
                      return Center(
                        child: CircularProgressIndicator(
                          backgroundColor: Colors.blueAccent,
                        ),
                      );
                    }
                    final vacations = snapshot.data.documents;
                    for (var vacation in vacations) {
                      final nm = vacation.data['pname'];
                      final dys = vacation.data['days'];
                      final pr = vacation.data['price'];
                      final imgc = vacation.data['imgcode'];
                      var pv = Destination(
                          days: dys, img: imgc, name: nm, price: pr);
                      places.add(pv);
                    }
                    return Container(
                      margin: EdgeInsets.only(left: 15),
                      height: 250,
                      child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: places.length,
                          itemBuilder: (_, index) {
                            return ImageCard(
                              dest: places[index],
                              name: places[index].name,
                              days: places[index].days,
                              img: places[index].img,
                            );
                          }),
                    );
                  }),
            ],
          ),
        ),
      ),
    );
  }
}

class CardTile extends StatelessWidget {
  final IconData icnsym;
  final String txt;
  final Color clr;

  CardTile({this.icnsym, this.txt, this.clr});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10, horizontal: 15),

      height: 57,
      width: 170,
      decoration:
          BoxDecoration(borderRadius: BorderRadius.circular(10), color: clr),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Icon(
            icnsym,
            size: 23,
            color: Colors.white,
          ),
          SizedBox(
            width: 7,
          ),
          Text(
            '$txt',
            style: TextStyle(
                fontFamily: 'Lato', fontSize: 18, color: Colors.white),
          )
        ],
      ),
      // child: Text(
      //   'Account',
      //   style: TextStyle(
      //       color: Colors.white, fontSize: 17, fontFamily: 'LatoB'),
      // ),
    );
  }
}
