import 'package:flutter/material.dart';

class CabPage extends StatefulWidget {
  @override
  _CabPageState createState() => _CabPageState();
}

class _CabPageState extends State<CabPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        child: Text(
          'Cab page',
          style: TextStyle(
            color: Colors.indigo[700],
            fontFamily: 'Lato',
            fontSize: 30,
          ),
        ),
      ),
    );
  }
}
