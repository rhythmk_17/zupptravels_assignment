import 'package:assignment_zupp/Screens/BusResult.dart';
import 'package:assignment_zupp/Screens/BusResultRound.dart';
import 'package:assignment_zupp/Screens/SeatSelection.dart';
import 'package:flutter/material.dart';
import 'package:assignment_zupp/widgets/constants.dart';
import 'package:assignment_zupp/Screens/BusPerson.dart';

class BusPage extends StatefulWidget {
  @override
  _BusPageState createState() => _BusPageState();
}

class _BusPageState extends State<BusPage> {
  bool day = true;
  bool jtypeo = true;
  bool ux = false;

  change() {
    setState(() {
      jtypeo = !jtypeo;
    });
    print(jtypeo);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Center(
            child: Card(
              elevation: 5,
              margin: EdgeInsets.all(20),
              child: Container(
                width: double.infinity,

                // decoration: BoxDecoration(
                //     borderRadius: BorderRadius.circular(15), color: Colors.cyan[300]),
                child: Container(
                  margin: EdgeInsets.symmetric(vertical: 25, horizontal: 30),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                ux = false;
                                jtypeo = true;
                              });
                            },
                            child: Container(
                              padding: EdgeInsets.all(7),
                              decoration: BoxDecoration(
                                border: Border.all(color: Color(0xFF37D2FF)),
                                color:
                                    jtypeo ? Color(0xFF37D2FF) : Colors.white,
                                borderRadius: BorderRadius.circular(25),
                              ),
                              child: Center(
                                child: Text(
                                  'ONE WAY',
                                  style: TextStyle(
                                    color: jtypeo
                                        ? Colors.white
                                        : Color(0xFF37D2FF),
                                    fontFamily: 'LatoB',
                                    fontSize: 12,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                ux = true;
                                jtypeo = false;
                              });
                            },
                            child: Container(
                              padding: EdgeInsets.all(7),
                              decoration: BoxDecoration(
                                border: Border.all(color: Color(0xFF37D2FF)),
                                color:
                                    jtypeo ? Colors.white : Color(0xFF37D2FF),
                                borderRadius: BorderRadius.circular(25),
                              ),
                              child: Center(
                                child: Text(
                                  'ROUND TRIP',
                                  style: TextStyle(
                                    color: jtypeo
                                        ? Color(0xFF37D2FF)
                                        : Colors.white,
                                    fontFamily: 'LatoB',
                                    fontSize: 12,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        'BOARDING POINT',
                        style: TextStyle(
                          color: Colors.black,
                          fontFamily: 'LatoB',
                          fontSize: 20,
                        ),
                      ),
                      SizedBox(
                        height: 17,
                      ),
                      Container(
                        height: 50,
                        child: TextFormField(
                          decoration: borderdecor.copyWith(
                            hintText: 'ENTER SOURCE',
                            hintStyle: TextStyle(
                              fontFamily: 'Lato',
                              fontWeight: FontWeight.w600,
                              fontSize: 15,
                            ),
                            prefixIcon: Icon(
                              Icons.location_city,
                              color: Color(0xFF2687EE),
                            ),
                            border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(7.0)),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Color(0xFF37D2FF), width: 1.5),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(7.0)),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Color(0xFF37D2FF), width: 2),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(7.0)),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        'DROPPING POINT',
                        style: TextStyle(
                          color: Colors.black,
                          fontFamily: 'LatoB',
                          fontSize: 20,
                        ),
                      ),
                      SizedBox(
                        height: 17,
                      ),
                      Container(
                        height: 50,
                        child: TextFormField(
                          decoration: borderdecor.copyWith(
                            hintText: 'ENTER DESTINATION',
                            hintStyle: TextStyle(
                              fontFamily: 'Lato',
                              fontWeight: FontWeight.w600,
                              fontSize: 15,
                            ),
                            prefixIcon: Icon(
                              Icons.place,
                              color: Color(0xFF2687EE),
                            ),
                            border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(7.0)),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Color(0xFF37D2FF), width: 1.5),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(7.0)),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Color(0xFF37D2FF), width: 2),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(7.0)),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            'JOURNEY DATE',
                            style: TextStyle(
                              color: Colors.black54,
                              fontFamily: 'LatoB',
                              fontSize: 11,
                            ),
                          ),
                          Text(
                            'RETURN DATE',
                            style: TextStyle(
                              color: Colors.black54,
                              fontFamily: 'LatoB',
                              fontSize: 11,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            '24 Apr,2020',
                            style: TextStyle(
                              color: Color(0xFF37D2FF),
                              fontFamily: 'LatoB',
                              fontSize: 15,
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                if (ux == false) {
                                  ux = true;
                                  jtypeo = false;
                                }
                              });
                            },
                            child: Text(
                              '29 Apr,2020',
                              style: TextStyle(
                                color: ux ? Color(0xFF37D2FF) : Colors.black45,
                                fontFamily: 'LatoB',
                                fontSize: 15,
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Center(
                        child: GestureDetector(
                          onTap: () {
                            if (jtypeo == true) {
                              Navigator.pushNamed(context, BusPager.id);
                            } else {
                              Navigator.pushNamed(context, BusResultRound.id);
                            }
                          },
                          child: Container(
                            width: 180,
                            height: 45,
                            decoration: BoxDecoration(
                              color: Color(0xFF37D2FF),
                              borderRadius: BorderRadius.circular(25),
                            ),
                            child: Center(
                              child: Text(
                                'SEARCH BUSES',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontFamily: 'LatoB',
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
