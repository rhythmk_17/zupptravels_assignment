import 'package:assignment_zupp/Screens/ReservPage.dart';
import 'package:flutter/material.dart';
import 'package:assignment_zupp/widgets/constants.dart';
import 'package:assignment_zupp/Screens/SeatSelection.dart';

class BusPerson extends StatefulWidget {
  static String id = 'bus-person';
  @override
  _BusPersonState createState() => _BusPersonState();
}

class _BusPersonState extends State<BusPerson> {
  @override
  bool notify = false;
  bool dialog = true;
  bool checkval = false;
  bool checkval2 = false;
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text(
            'Traveller info',
            style: TextStyle(color: Colors.black, fontFamily: 'LatoB'),
          ),
          backgroundColor: Colors.white,
          elevation: 0,
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios,
              size: 22,
              color: Colors.black,
            ),
            onPressed: () {},
          ),
        ),
        body: SingleChildScrollView(
          child: SafeArea(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                dialog
                    ? Container(
                        // height: 70,
                        color: Colors.cyan[50],
                        padding: EdgeInsets.only(
                            left: 10, right: 10, bottom: 10, top: 10),
                        child: Column(
                          children: <Widget>[
                            Text(
                              'Would you like to get updates on new deals and get email notifications?',
                              style: TextStyle(
                                color: Colors.black45,
                                fontFamily: 'Lato',
                                fontSize: 15,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(right: 20),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        notify = true;
                                        dialog = false;
                                      });
                                      print(notify);
                                    },
                                    child: Text(
                                      'YES',
                                      style: TextStyle(
                                        color: Colors.blue,
                                        fontFamily: 'LatoB',
                                        fontSize: 15,
                                      ),
                                    ),
                                  ),
                                  SizedBox(width: 30),
                                  GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        notify = false;
                                        dialog = false;
                                      });
                                      print(notify);
                                    },
                                    child: Text(
                                      'NO',
                                      style: TextStyle(
                                        color: Colors.blue,
                                        fontFamily: 'LatoB',
                                        fontSize: 15,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      )
                    : Container(
                        child: null,
                      ),
                Container(
                  margin: EdgeInsets.all(15),
                  padding: EdgeInsets.only(bottom: 5),
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(
                        color: Color(0xFF37D2FF),
                        width: 1.5,
                      ),
                    ),
                  ),
                  child: Text(
                    'Personal Details',
                    style: TextStyle(
                      color: Colors.black,
                      fontFamily: 'LatoB',
                      fontSize: 23,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(15),
                  child: Text(
                    '#Traveller 1',
                    style: TextStyle(
                      color: Colors.black45,
                      fontFamily: 'LatoB',
                      fontSize: 18,
                    ),
                  ),
                ),
                Container(
                  margin:
                      EdgeInsets.only(left: 15, right: 50, top: 15, bottom: 10),
                  height: 50,
                  child: TextFormField(
                    decoration: borderdecor.copyWith(
                      hintText: 'Enter Name',
                      hintStyle: TextStyle(
                        fontFamily: 'Lato',
                        color: Colors.black45,
                        fontWeight: FontWeight.w600,
                        fontSize: 15,
                      ),
                      prefixIcon: Icon(
                        Icons.person,
                        color: Color(0xFF2687EE),
                      ),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(7.0)),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Color(0xFF37D2FF), width: 1.5),
                        borderRadius: BorderRadius.all(Radius.circular(7.0)),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Color(0xFF37D2FF), width: 2),
                        borderRadius: BorderRadius.all(Radius.circular(7.0)),
                      ),
                    ),
                  ),
                ),
                Container(
                  width: 120,
                  margin:
                      EdgeInsets.only(left: 15, right: 50, top: 5, bottom: 10),
                  height: 50,
                  child: TextFormField(
                    decoration: borderdecor.copyWith(
                      hintText: ' Age',
                      hintStyle: TextStyle(
                        fontFamily: 'Lato',
                        color: Colors.black45,
                        fontWeight: FontWeight.w600,
                        fontSize: 15,
                      ),
                      prefixIcon: Icon(
                        Icons.format_list_numbered,
                        color: Color(0xFF2687EE),
                      ),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(7.0)),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Color(0xFF37D2FF), width: 1.5),
                        borderRadius: BorderRadius.all(Radius.circular(7.0)),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Color(0xFF37D2FF), width: 2),
                        borderRadius: BorderRadius.all(Radius.circular(7.0)),
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                    left: 15,
                    right: 50,
                  ),
                  child: Row(
                    children: <Widget>[
                      Checkbox(
                        checkColor: Colors.white,
                        activeColor: Color(0xFF37D2FF),
                        value: checkval,
                        onChanged: (bool val) {
                          print(val);
                          setState(() {
                            checkval = val;
                          });
                        },
                      ),
                      Text(
                        'Save Traveller details',
                        style: TextStyle(
                          color: Colors.black45,
                          fontFamily: 'Lato',
                          fontSize: 13,
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 15, horizontal: 25),
                  child: Divider(
                    thickness: 1.5,
                    height: 1,
                    color: Colors.black12,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(15),
                  child: Text(
                    '#Traveller 2',
                    style: TextStyle(
                      color: Colors.black45,
                      fontFamily: 'LatoB',
                      fontSize: 18,
                    ),
                  ),
                ),
                Container(
                  margin:
                      EdgeInsets.only(left: 15, right: 50, top: 15, bottom: 10),
                  height: 50,
                  child: TextFormField(
                    decoration: borderdecor.copyWith(
                      hintText: 'Enter Name',
                      hintStyle: TextStyle(
                        fontFamily: 'Lato',
                        color: Colors.black45,
                        fontWeight: FontWeight.w600,
                        fontSize: 15,
                      ),
                      prefixIcon: Icon(
                        Icons.person,
                        color: Color(0xFF2687EE),
                      ),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(7.0)),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Color(0xFF37D2FF), width: 1.5),
                        borderRadius: BorderRadius.all(Radius.circular(7.0)),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Color(0xFF37D2FF), width: 2),
                        borderRadius: BorderRadius.all(Radius.circular(7.0)),
                      ),
                    ),
                  ),
                ),
                Container(
                  width: 120,
                  margin:
                      EdgeInsets.only(left: 15, right: 50, top: 5, bottom: 10),
                  height: 50,
                  child: TextFormField(
                    decoration: borderdecor.copyWith(
                      hintText: ' Age',
                      hintStyle: TextStyle(
                        fontFamily: 'Lato',
                        color: Colors.black45,
                        fontWeight: FontWeight.w600,
                        fontSize: 15,
                      ),
                      prefixIcon: Icon(
                        Icons.format_list_numbered,
                        color: Color(0xFF2687EE),
                      ),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(7.0)),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Color(0xFF37D2FF), width: 1.5),
                        borderRadius: BorderRadius.all(Radius.circular(7.0)),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Color(0xFF37D2FF), width: 2),
                        borderRadius: BorderRadius.all(Radius.circular(7.0)),
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                    left: 15,
                    right: 50,
                  ),
                  child: Row(
                    children: <Widget>[
                      Checkbox(
                        checkColor: Colors.white,
                        activeColor: Color(0xFF37D2FF),
                        value: checkval2,
                        onChanged: (bool val) {
                          print(val);
                          setState(() {
                            checkval2 = val;
                          });
                        },
                      ),
                      Text(
                        'Save Traveller details',
                        style: TextStyle(
                          color: Colors.black45,
                          fontFamily: 'Lato',
                          fontSize: 13,
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 15, horizontal: 25),
                  child: Divider(
                    thickness: 1.5,
                    height: 1,
                    color: Colors.black12,
                  ),
                ),
                Container(
                  margin:
                      EdgeInsets.only(left: 15, right: 50, top: 15, bottom: 10),
                  height: 50,
                  child: TextFormField(
                    decoration: borderdecor.copyWith(
                      hintText: 'Enter your mobile number',
                      hintStyle: TextStyle(
                        fontFamily: 'Lato',
                        color: Colors.black45,
                        fontWeight: FontWeight.w600,
                        fontSize: 15,
                      ),
                      prefixIcon: Icon(
                        Icons.phone,
                        color: Color(0xFF2687EE),
                      ),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(7.0)),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Color(0xFF37D2FF), width: 1.5),
                        borderRadius: BorderRadius.all(Radius.circular(7.0)),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Color(0xFF37D2FF), width: 2),
                        borderRadius: BorderRadius.all(Radius.circular(7.0)),
                      ),
                    ),
                  ),
                ),
                Container(
                  margin:
                      EdgeInsets.only(left: 20, right: 50, top: 5, bottom: 10),
                  height: 50,
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.email, size: 24, color: Color(0xFF2687EE)),
                      SizedBox(width: 13),
                      Text(
                        'Kachhadiyaridham787@gmail.com',
                        style: TextStyle(
                          color: Colors.black,
                          fontFamily: 'LatoB',
                          fontSize: 16,
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 20),
                Center(
                  child: GestureDetector(
                    onTap: () {
                      Navigator.pushNamed(context, ReservPage.id);
                    },
                    child: Container(
                      width: 180,
                      height: 45,
                      decoration: BoxDecoration(
                        color: Color(0xFF37D2FF),
                        borderRadius: BorderRadius.circular(25),
                      ),
                      child: Center(
                        child: Text(
                          'CONFIRM',
                          style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'LatoB',
                            fontSize: 18,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 20),
              ],
            ),
          ),
        ));
  }
}
