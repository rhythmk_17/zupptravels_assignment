import 'package:flutter/material.dart';

class VacationPage extends StatefulWidget {
  static String id = 'vaca-page';
  @override
  _VacationPageState createState() => _VacationPageState();
}

class _VacationPageState extends State<VacationPage> {
  List<Widget> containers = [
    Container(
      color: Colors.red,
    ),
    Container(
      color: Colors.deepPurple,
    ),
    Container(
      color: Colors.blue,
    ),
    Container(
      color: Colors.green,
    ),
  ];
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          bottom: TabBar(
            indicatorColor: Colors.indigo[700],
            tabs: <Widget>[
              Tab(
                child: Text(
                  'TOUR OVERVIEW',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.white, fontFamily: 'LatoB', fontSize: 12),
                ),
              ),
              Tab(
                child: Text(
                  'ITNERARY',
                  style: TextStyle(
                      color: Colors.white, fontFamily: 'LatoB', fontSize: 12),
                ),
              ),
              Tab(
                child: Center(
                  child: Text(
                    'TOUR DETAILS',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.white, fontFamily: 'LatoB', fontSize: 12),
                  ),
                ),
              ),
              Tab(
                child: Center(
                  child: Text(
                    'T & C',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.white, fontFamily: 'LatoB', fontSize: 12),
                  ),
                ),
              ),
            ],
          ),
          title: Text(
            'Holiday details',
            style: TextStyle(
                color: Colors.black, fontFamily: 'LatoB', fontSize: 20),
          ),
          backgroundColor: Color(0xFF37D2FF),
          elevation: 0,
        ),
        body: TabBarView(children: containers),
      ),
    );
  }
}

class DetailsVac extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        height: 400,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(15),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 2,
              blurRadius: 4,
            ),
          ],
        ),
        margin: EdgeInsets.all(15),
        child: Column(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                color: Color(0xFF37D2FF),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(15),
                    topRight: Radius.circular(15)),
              ),
              height: 50,
              child: Center(
                child: Text(
                  'LOWEST PRICE GUARANTEE',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.white, fontFamily: 'LatoB', fontSize: 17),
                ),
              ),
            ),
            Container(
              height: 50,
              child: Center(
                child: Text(
                  'INR 5000',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Color(0xFF37D2FF),
                      fontFamily: 'LatoB',
                      fontSize: 17),
                ),
              ),
            ),
            Divider(
              height: 0,
              thickness: 1,
            ),
          ],
        ));
  }
}
