import 'package:assignment_zupp/Screens/FlightPage.dart';
import 'package:assignment_zupp/Screens/HomePage.dart';
import 'package:flutter/material.dart';
import 'package:assignment_zupp/Screens/NewHomepage.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:assignment_zupp/Screens/BusPage.dart';
import 'package:assignment_zupp/Screens/CabPage.dart';
import 'package:assignment_zupp/Screens/HotelPage.dart';
import 'package:assignment_zupp/Screens/HolidaysPage.dart';

class AppDrawer extends StatefulWidget {
  static String id = 'app-Drawer';
  @override
  _AppDrawerState createState() => _AppDrawerState();
}

class _AppDrawerState extends State<AppDrawer> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  @override
  int _currState = 0;
  final tabs = [
    HolidaysPage(),
    HotelPage(),
    FlightPage(),
    BusPage(),
    CabPage()
  ];
  GlobalKey _bottomNavigationKey = GlobalKey();

  Widget build(BuildContext context) {
    return Scaffold(
      drawer: new Drawer(
          child: Column(
        children: <Widget>[
          SizedBox(
            height: 200,
          ),
          Padding(
            padding: EdgeInsets.only(top: 20),
            child: ListTile(
              leading: CircleAvatar(
                backgroundColor: Colors.blue,
                radius: 30,
              ),
              title: Text(
                'Ridham',
                style: TextStyle(
                    fontFamily: 'LatoB', fontSize: 22, color: Colors.black),
              ),
              subtitle: Padding(
                padding: EdgeInsets.only(top: 5),
                child: Text(
                  '7622978174',
                  style: TextStyle(
                      fontFamily: 'Lato', fontSize: 17, color: Colors.black54),
                ),
              ),
            ),
          ),
          SizedBox(height: 35),
          Padding(
            padding: EdgeInsets.only(left: 30, top: 7, bottom: 7),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Icon(
                  Icons.people,
                  size: 25,
                  color: Color(0xFF5D6DFF),
                ),
                SizedBox(
                  width: 20,
                ),
                Text(
                  'My Account',
                  style: TextStyle(
                      fontFamily: 'LatoB', fontSize: 17, color: Colors.black54),
                )
              ],
            ),
          ),
          Divider(
            thickness: 1,
          ),
          // SizedBox(height: 20),
          Padding(
            padding: EdgeInsets.only(left: 30, top: 7, bottom: 7),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Icon(
                  Icons.assessment,
                  size: 25,
                  color: Color(0xFF37D2FF),
                ),
                SizedBox(
                  width: 20,
                ),
                Text(
                  'Print Ticket',
                  style: TextStyle(
                      fontFamily: 'LatoB', fontSize: 17, color: Colors.black54),
                )
              ],
            ),
          ),
          Divider(
            thickness: 1,
          ),
          // SizedBox(height: 20),
          Padding(
            padding: EdgeInsets.only(left: 30, top: 7, bottom: 7),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Icon(
                  Icons.card_membership,
                  size: 25,
                  color: Color(0xFF2687EE),
                ),
                SizedBox(
                  width: 20,
                ),
                Text(
                  'Redeem Gift card',
                  style: TextStyle(
                      fontFamily: 'LatoB', fontSize: 17, color: Colors.black54),
                )
              ],
            ),
          ),
          Divider(
            thickness: 1,
          ),
        ],
      )),
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      //  drawer: AppDrawer(),
      appBar: AppBar(
        title: Text(
          'ZuppTravel',
          style: TextStyle(color: Colors.cyan, fontFamily: 'LatoB'),
        ),
        backgroundColor: Colors.white,
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.menu,
            size: 25,
            color: Colors.black,
          ),
          onPressed: () {},
        ),
        actions: <Widget>[
          IconButton(
            onPressed: () {},
            icon: Icon(
              Icons.monetization_on,
              size: 25,
              color: Color(0xFF37D2FF),
            ),
          ),
          IconButton(
            onPressed: () {},
            icon: Icon(
              Icons.card_giftcard,
              size: 25,
              color: Color(0xFF2687EE),
            ),
          ),
          IconButton(
            onPressed: () {},
            icon: Icon(
              Icons.enhanced_encryption,
              size: 25,
              color: Color(0xFF5D6DFF),
            ),
          ),
          IconButton(
            icon: Icon(
              Icons.person_outline,
              color: Colors.black,
              size: 25,
            ),
            onPressed: () {
              _scaffoldKey.currentState.openDrawer();
              // AppDrawer();
              // do something
            },
          ),
        ],
      ),

      body: tabs[_currState],
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.white,
        currentIndex: _currState,
        key: _bottomNavigationKey,
        elevation: 0,
        selectedItemColor: Colors.lightBlue,
        iconSize: 27,
        // showSelectedLabels: false,
        // showUnselectedLabels: false,
        // type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
            icon: Icon(
              Icons.place,
              color: Colors.indigo[700],
            ),
            //   backgroundColor: logoc,
            title: Text(
              'Holidays',
              style: TextStyle(
                color: Colors.indigo[700],
                fontFamily: 'LatoB',
                fontSize: 12,
              ),
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.business, color: Colors.indigo[700]),
            //   backgroundColor: logoc,
            title: Text(
              'Hotels',
              style: TextStyle(
                color: Colors.indigo[700],
                fontFamily: 'LatoB',
                fontSize: 12,
              ),
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.flight, color: Colors.indigo[700]),
            //   backgroundColor: logoc,
            title: Text(
              'Fights',
              style: TextStyle(
                color: Colors.indigo[700],
                fontFamily: 'LatoB',
                fontSize: 12,
              ),
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.directions_bus, color: Colors.indigo[700]),
            // backgroundColor: logoc,
            title: Text(
              'Bus',
              style: TextStyle(
                color: Colors.indigo[700],
                fontFamily: 'LatoB',
                fontSize: 12,
              ),
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.local_taxi, color: Colors.indigo[700]),
            //backgroundColor: logoc,
            title: Text(
              'Cabs',
              style: TextStyle(
                color: Colors.indigo[700],
                fontFamily: 'LatoB',
                fontSize: 12,
              ),
            ),
          ),
        ],
        onTap: (index) {
          setState(() {
            _currState = index;
          });
        },
      ),
    );
  }
}
