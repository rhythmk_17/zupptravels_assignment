import 'package:assignment_zupp/Screens/AppDrawer.dart';
import 'package:assignment_zupp/Screens/BusPage.dart';
import 'package:assignment_zupp/Screens/BusPerson.dart';
import 'package:assignment_zupp/Screens/BusResult.dart';
import 'package:assignment_zupp/Screens/BusResultRound.dart';
import 'package:assignment_zupp/Screens/ReservPage.dart';
import 'package:assignment_zupp/Screens/SeatSelection.dart';
import 'package:assignment_zupp/Screens/VacationFirst.dart';
import 'package:assignment_zupp/Screens/VacationPage.dart';
import 'package:flutter/material.dart';
import 'Screens/Landingpage.dart';
import 'Screens/HomePage.dart';
import 'Screens/AppDrawer.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: LandingPage.id,
      routes: {
        LandingPage.id: (context) => LandingPage(),
        // CartPage.id:(context)=>CartPage(),
        // ItemRegister.id:(context)=>ItemRegister(),
        Homepage.id: (context) => Homepage(),
        AppDrawer.id: (context) => AppDrawer(),
        SeatSelection.id: (context) => SeatSelection(),
        BusPager.id: (context) => BusPager(),
        ReservPage.id: (context) => ReservPage(),
        BusPerson.id: (context) => BusPerson(),
        BusResultRound.id: (context) => BusResultRound(),
        VacationPage.id: (context) => VacationPage(),
        VacationFirst.id: (context) => VacationFirst(),
        // UserPage.id:(context)=>UserPage(),
        // AppDrawer.id:(context)=>AppDrawer(),
        // CustomerReceipt.id:(context)=>CustomerReceipt(),
        // SplashScreen.id:(context)=>SplashScreen(),
        // RegisterUser.id:(context)=>RegisterUser(),
      },
    );
  }
}
