import 'package:flutter/material.dart';

class IconTile extends StatelessWidget {
  final IconData iconsym;
  final String des;

  IconTile({this.iconsym, this.des});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 75,
      child: Column(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              color: Colors.indigo[100],
              borderRadius: BorderRadius.circular(10),
            ),
            child: IconButton(
              icon: Icon(
                iconsym,
                size: 25,
                color: Colors.indigo[700],
              ),
              onPressed: () {},
            ),
          ),
          SizedBox(
            height: 5,
          ),
          Text(
            '$des',
            style: TextStyle(fontFamily: 'LatoB', fontSize: 12),
          )
        ],
      ),
    );
  }
}
