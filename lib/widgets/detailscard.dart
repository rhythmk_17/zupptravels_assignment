import 'package:flutter/material.dart';

class detail_tile extends StatelessWidget {
  final IconData iconsym;
  final String des;
  final Color color;

  detail_tile({this.iconsym, this.des, this.color});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      width: 150,
      child: Container(
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(50),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Icon(
              iconsym,
              size: 20,
              color: Colors.indigo[700],
            ),
            SizedBox(
              width: 7,
            ),
            Text(
              '$des',
              style: TextStyle(fontFamily: 'Lato', fontSize: 15),
            )
          ],
        ),
      ),
    );
  }
}
